# 2021 Q2 Objective: Improve Trading Skills

## Key Results

- %1

## Retrospective

### Good

<!-- Things that went well. If your team accomplished your goal, what contributed to the team's success? -->

### Bad

<!-- 
Things that did not go well, but are generally one-off events—things that we don’t expect to repeat. If your team didn’t accomplish your goal, what obstacles were encountered? 

e.g. Given the high number of OOO and the realignment changes, the MR Rate should have been reduced by at least 30%. This rate was changed during the quarter. We have to do a better job anticipating how significant OOO impacts our MR Rates.
Do Better Things that we can do better next time. These can include a suggestion on how to do it better.
-->

### Best

<!-- Things that went really well. Celebrate! How can we do more of this? -->

### Feels + Open Questions

<!-- Emotions, mindsets, areas of confusion, and opportunities to consider. -->

### Additional Questions

<!--
Was the goal harder or easier to achieve than you’d thought when you set it?
If we were to rewrite the goal, what would we change?
What have we learned that might alter our approach for our next cycle’s OKRs?
-->

## Schedule

### Month 1

- Week 1 - This week EMs are scoring OKRs from the previous quarter that may not have been scored.
    - [ ] Update the final OKR scores from the previous quarter
    - [ ] Complete the OKR Retrospective from the previous quarter
    - [ ] Close out the previous quarters’ OKRs
- Week 4
    - [ ] Update the current progress (scores)
    - [ ] Update OKRs if priorities have recently changed or if you realize they are unrealistic
    - [ ] Remove OKRs if priorities have recently changed
    - [ ] Remove OKRs if priorities have recently changed
    - [ ] Add new OKRs if priorities have recently changed

### Month 2

- Week 4
    - [ ] Updating current progress (scores)
    - [ ] Update OKRs if priorities have recently changed or if you realize they are unrealistic
    - [ ] Remove OKRs if priorities have recently changed
    - [ ] Add new OKRs if priorities have recently changed

### Month 3

- Week 2
    - [ ] Begin working on the next quarters’ OKRs
- Week 4
    - [ ] Begin finalizing the OKR score updates for the current quarter
    - [ ] Finalize the next quarters’ OKRs.

Inspired by: https://about.gitlab.com/handbook/engineering/development/dev/create/engineering-managers/okrs/
